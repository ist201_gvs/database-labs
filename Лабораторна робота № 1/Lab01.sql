create database Students

create table TypeOfStudy
(
   id_type INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_type PRIMARY KEY,
   name VARCHAR(20) NOT NULL,
);

create table Specialty
(
   id_specialty INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_specialty PRIMARY KEY,
   NumberSpecialty INT NOT NULL,
   NameSpecialty VARCHAR(50) NOT NULL,
);

create table Chair
(
   id_chair INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_chair PRIMARY KEY,
   NameChair VARCHAR(50) NOT NULL,
);

create table Groups
(
   id_group INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_groups PRIMARY KEY,
   NameGroup VARCHAR(50) NOT NULL,
);

create table ReasonOfDeduction
(
   id_reason INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_reason PRIMARY KEY,
   NameReason VARCHAR(50) NOT NULL,
);

create table Students
(
   id_student INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_id_student PRIMARY KEY,
   firstname VARCHAR(50) NOT NULL,
   lastname VARCHAR(50) NOT NULL,
   adress VARCHAR(100) NOT NULL,
   entryYear INT NOT NULL,
   gradebook INT NOT NULL,
   dateDeduction DATE,
   id_type INT NOT NULL,
   id_specialty INT NOT NULL,
   id_chair INT NOT NULL,
   id_group INT NOT NULL,
   id_reason INT NOT NULL,
   FOREIGN KEY (id_type) REFERENCES TypeOfStudy (id_type),
   FOREIGN KEY (id_specialty) REFERENCES Specialty (id_specialty),
   FOREIGN KEY (id_chair) REFERENCES Chair (id_chair),
   FOREIGN KEY (id_group) REFERENCES Groups (id_group),
   FOREIGN KEY (id_reason) REFERENCES ReasonOfDeduction (id_reason)
);

select * from Chair
select * from ReasonOfDeduction
select * from Specialty
select * from TypeOfStudy
select * from Students
select * from Groups

Insert into Chair (NameChair)
values ('������� ������� ����������� ������������'), 
('������� ��������� ������� �� ���������������'),
('������� ���������� ������� �� �����������'),
('������� ����������� ����')

Insert into Groups (NameGroup)
values ('���-20-1'),
('��-20-1'),
('ʲ-20-1'),
('���-20-1'),
('��-20-1'),
('��-20-1')

Insert into ReasonOfDeduction (NameReason)
values ('�� ������'),
('��������� �����������'),
('����� �������������� ������ �������� � ��������'),
('��������� ���������')

Insert into Specialty (NumberSpecialty, NameSpecialty)
values ('121', '��������� ����������� �������������'),
('122', '����������� �����'),
('123', '����������� ���������'),
('125', '�ʳ����������'),
('126', '������������� ������� � �������㳿�')

Insert into TypeOfStudy (name)
values ('����� �����'),
('������ �����')

--1
go
create trigger UpdateTask1 on Groups for update
as
begin
update Information set CountUpdate = CountUpdate + 1
print '�������� Groups ������'
end

update Groups set NameGroup = '��-21-1' where id_group = 5
select * from Information
select * from Groups


--2
go
create trigger UpdateTask2 on Students after update
as
BEGIN
declare @id_student int
select @id_student = @id_student from inserted
if @id_student = (select id_student from Students where
id_student = @id_student)
begin
print '�������� �� ������'
ROLLBACK TRANSACTION
end;
ELSE
print '�������� ������'
END;

update Students set dateDeduction = '2022-06-27', id_reason = 4
where id_student = 1

--3
go
alter trigger DeleteTask1 on Groups for delete
as
begin
 declare @id_group int
 select @id_group = id_group from deleted
 DELETE FROM Students where Students.id_group = @id_group
end

delete from Groups 
where id_group = 3

--4

-- ���������

--1
create table countRow 
(
   tableName varchar(30),
   countRow int
)

go
alter proc ProcCountRow
as
exec sp_MSForEachTable @command1 = 'INSERT countRow (tableName, countRow) SELECT ''?'', COUNT(*) FROM ?'
delete from countRow where tableName like '%sysdiagrams%' or tableName like '%counts%'

exec ProcCountRow

select * from countRow

--2
create table countField
(
   countField int
)

go
alter proc ProcCountField
as 
insert countField(countField) select COUNT(*) from INFORMATION_SCHEMA.COLUMNS
where TABLE_NAME !='sysdiagrams' and TABLE_NAME !='countField'

exec ProcCountField

select * from countField

--3
go
create proc UniqueValue
as
declare @count nvarchar(50)
select @count = COUNT(DISTINCT id_student) from Students
print ('ʳ������ ���������� ������� id_student = ' + @count)
select @count = COUNT(DISTINCT firstname) from Students
print ('ʳ������ ���������� ������� firstname = ' + @count)
select @count = COUNT(DISTINCT lastname) from Students
print ('ʳ������ ���������� ������� lastname = ' + @count)
select @count = COUNT(DISTINCT adress) from Students
print ('ʳ������ ���������� ������� adress = ' + @count)
select @count = COUNT(DISTINCT entryYear) from Students
print ('ʳ������ ���������� ������� entryYear = ' + @count)
select @count = COUNT(DISTINCT gradebook) from Students
print ('ʳ������ ���������� ������� gradebook = ' + @count)
select @count = COUNT(DISTINCT dateDeduction) from Students
print ('ʳ������ ���������� ������� dateDeduction = ' + @count)
select @count = COUNT(DISTINCT id_type) from Students
print ('ʳ������ ���������� ������� id_type = ' + @count)
select @count = COUNT(DISTINCT id_specialty) from Students
print ('ʳ������ ���������� ������� id_specialty = ' + @count)
select @count = COUNT(DISTINCT id_chair) from Students
print ('ʳ������ ���������� ������� id_chair = ' + @count)
select @count = COUNT(DISTINCT id_group) from Students
print ('ʳ������ ���������� ������� id_group = ' + @count)
select @count = COUNT(DISTINCT id_reason) from Students
print ('ʳ������ ���������� ������� id_reason = ' + @count)

exec UniqueValue


-- last 
go
alter trigger InsertTask1 on Students after insert
as
begin
update Information set CountInsert = CountInsert + 1 where NameTable like '%Students%'
end;

insert into Students (firstname, lastname, adress, entryYear, gradebook, id_type, id_specialty, id_chair, id_group)
values ('�������', '������', '�. ������', 2021, 25, 1, 3,  2, 1)

select * from Information
select * from Students

insert into Information (NameTable)
values ('Students')


go
alter trigger DeleteTask2 on Students
after delete
as
declare @id_student int
select @id_student = id_student from deleted
if not exists (select * from Students where Students.id_student = @id_student and dateDeduction != NULL and id_reason != NULL)
begin
print('�������� �� �������� !')
rollback transaction
end;
else
begin
print('�������� ��������')
end;

delete from Students where id_student = 3


