select @@SERVERNAME

create database Lab04

use Lab04

create table stud 
(
  ID int primary key identity (1,1),
  NameStud varchar(30) not null,
  GroupStud varchar(15) not null,
);

select * from stud

Insert into stud (NameStud, GroupStud)
values ('����������� �.�.', '���-20-1'),
 ('����. �.�.', '���-20-1'),
 ('�������� �.�.', '���-20-1'),
 ('������ �.�.', '��-20-1'),
 ('������� �.�.', 'ʲ-20-1'),
 ('������� �.�.', 'ʲ-20-1'),
 ('���� �.�.', '���-20-1'),
 ('������� �.�.', '���-20-1'),
 ('������� �.�.', '���-20-2'),
 ('����������� �.�.', '���-20-3')

 go
 alter proc proc1
 as
 select * from stud


 go
 create proc ProcInsert
 (
 @NameStud nvarchar(30),
 @GroupStud nvarchar(30)
 )
 as
 begin
 insert into stud (NameStud, GroupStud)
 values (@NameStud, @GroupStud)
 select * from stud
 end

 go
 alter proc ProcDelete
 (
  @ID int
 )
 as
 begin
 delete from stud where NameStud like @ID;
 select * from stud
 end


 go
 alter proc ProcUpdate
 (
 @key1 varchar(30), 
 @key2 varchar(30), 
 @value1 varchar(30),
 @value2 varchar(30)
 )
 as
 begin
 update stud set @key1 = @value1 where @key2 = @value2
 select * from stud
 end