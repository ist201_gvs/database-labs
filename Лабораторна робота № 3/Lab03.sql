-- ����������� ������ � 3

-- �������� ���� �������

SELECT SP1.name AS ServerRoleName, 
 isnull (SP2.name, 'No members') AS LoginName   
 FROM sys.server_role_members AS SRM
 RIGHT OUTER JOIN sys.server_principals AS SP1
   ON SRM.role_principal_id = SP1.principal_id
 LEFT OUTER JOIN sys.server_principals AS SP2
   ON SRM.member_principal_id = SP2.principal_id
 WHERE SP1.is_fixed_role = 1

 -- ��������� ������� ��

 create database Users

 use Users

 create table Users
 (
    firstName varchar(30),
	lastName varchar(30),
	age int,
	phone varchar(15)
 );

 -- ������������ ���� ����� Users

BACKUP DATABASE Users
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_FullDbBkup.bak' 
WITH INIT, NAME = 'Users Full Db backup',
DESCRIPTION = 'Users Full Database Backup'

-- ³��������� ���� ����� Users

RESTORE DATABASE Users
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_FullDbBkup.bak'
WITH RECOVERY, REPLACE

-- ����� ������������ � ��������

BACKUP DATABASE Users
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_FullDbBkup.bak'
WITH INIT, NAME = 'Users Full Db backup',
DESCRIPTION = 'Users Full Database Backup'

-- ������������ �������

BACKUP LOG Users
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_TlogBkup.bak'
WITH NOINIT, NAME = 'Users Translog backup',
DESCRIPTION = 'Users Transaction Log Backup', NOFORMAT


-- ������������ ���������� ��������� �������

BACKUP LOG Users 
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_TaillogBkup.bak'
WITH NORECOVERY

-- ����� ���������� � ����� �������� ��ﳿ 

RESTORE DATABASE Users
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_FullDbBkup.bak'
WITH NORECOVERY

-- ³��������� ��� ��������� ���� ������� ����������

RESTORE LOG Users
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_TlogBkup.bak'
WITH NORECOVERY


-- ���������� ��������� �������� ��ﳿ 

BACKUP DATABASE Users
TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_DiffDbBkup.bak'
WITH INIT, DIFFERENTIAL, NAME = 'Users Diff Db backup',
DESCRIPTION = 'Users Differential Database Backup'

-- ³��������� �������� ��ﳿ 

RESTORE DATABASE Users
FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\Backup\Users_DiffDbBkup.bak'
WITH NORECOVERY